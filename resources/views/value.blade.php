<?php

use Illuminate\View\ComponentAttributeBag;

/** @var ComponentAttributeBag $attributes */

?>

<span {{$attributes}}>{{$computed_value($slot)}}</span>

